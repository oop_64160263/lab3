import java.util.Scanner;

public class Problem3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int Num1,Num2;
        System.out.print("Please input first number:");
        Num1 = sc.nextInt();
        System.out.print("Please input second number:");
        Num2 = sc.nextInt();
        if(Num1<Num2){
            for(int i=Num1; i<=Num2; i++){
                System.out.print(i+" ");
            }
        }else{
            System.out.println("Error");
            for(int i=Num1; i>=Num2; i--){
                System.out.print(i +" ");
            }
        }
        sc.close();
    }
}
