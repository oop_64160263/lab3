import java.util.Scanner;

public class Problem5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please input: ");
            String text = sc.nextLine();
            System.out.println(text);
            if(text.trim().equalsIgnoreCase("bye")){
                System.out.println("Exit Program");
                System.exit(0);
            }   
        }
    }
}
